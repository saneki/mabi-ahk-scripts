#IfWinActive Mabinogi
#SingleInstance Force

March = 1
Lullaby = 2
EnduringMelody = 3
Vivace = 4
Battlefield = 5

; Checks to make sure this script is being run in admin mode
if not A_IsAdmin {
   Run *RunAs "%A_ScriptFullPath%"  ; Requires v1.0.92.01+
   ExitApp
}

; Bard training ftw

PlayMusic(SkillKey, WaitTime)
{
	SendInput % "{" . SkillKey . "}"
	Sleep WaitTime
	SendInput {ESC down}
	Sleep 40
	SendInput {ESC up}
	Sleep 200
}

Home::
	Loop {
		; Note: Lullaby doesn't actually give exp for renown, but leaving it in
		; anyways because the timing works.
		PlayMusic(March, 1000)
		PlayMusic(Lullaby, 1400)
		PlayMusic(EnduringMelody, 1400)
		Sleep 1000
		PlayMusic(March, 1000)
		PlayMusic(Lullaby, 1400)
		PlayMusic(Vivace, 1400)
		Sleep 1000
		PlayMusic(March, 1000)
		PlayMusic(Lullaby, 1400)
		PlayMusic(Battlefield, 1400)
		Sleep 1000
	}
	Return

PgDn::
	Pause
	Return

END::
	ExitApp
	Return

F12::
	Reload
	Return

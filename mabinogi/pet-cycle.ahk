global DesummonKey  := "``"
global MaxCooldown  := 62
global PetCooldowns := {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
global TrayTipTitle := "Pet Cycle"
global Summoned     := 0

CountdownCallback()
{
	For Index, Cooldown in PetCooldowns {
		if (Cooldown > 0)
			PetCooldowns[Index]--
	}
}

DesummonPet(ShouldSleep = True)
{
	SendInput % "{" . DesummonKey . "}"
	PetCooldowns[Summoned] := MaxCooldown
	Summoned := 0
	if (ShouldSleep)
		Sleep 750
}

SummonPet(Index, ShouldSleep = True)
{
	SendInput % "{Numpad" . Index . "}"
	Summoned := Index
	if (ShouldSleep)
		Sleep 750
}

SummonNextAvailablePet()
{
	For Index, Cooldown in PetCooldowns {
		if (Cooldown == 0) {
			SummonPet(Index)
			Return True
		}
	}
	Return False
}

SummonOrDesummonPet()
{
	if (Summoned == 0)
		Return SummonNextAvailablePet()
	else {
		DesummonPet()
		Return True
	}
}

GetNextSummonTime()
{
	Time := -1
	For Index, Cooldown in PetCooldowns {
		if (Time == -1 || Cooldown < Time)
			Time := Cooldown
	}
	Return Time
}

Info(Str)
{
	TrayTip % TrayTipTitle, %Str%, 1000
}

SetTimer, CountdownCallback, 1000

HOME::
	if (!SummonOrDesummonPet())
		Info("Wait a bit to summon next pet: " . GetNextSummonTime() . " seconds")
	Return

DELETE::
	Reload

END::
	ExitApp

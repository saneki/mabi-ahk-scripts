; Description:
;   Small script designed to slaughter foxes around Trefor with the Dual Guns
;   Mastery skill. Shoot, move target, shoot, move target, repeat.

global SkillKey = "1"
global MoveTargetKey = "TAB"

UseSkillOnNextTarget()
{
	SendInput % "{" . MoveTargetKey . "}"
	Sleep 500
	SendInput % "{" . SkillKey . "}"
	Sleep 500
}

Main()
{
	Loop {
		UseSkillOnNextTarget()
		Sleep 1000
	}
}

HOME::
	Main()
	Return

DELETE::
	Reload

END::
	ExitApp
